## Overview
* A modern, lightweight & responsive styleguide based on Flexbox, created for SwaadBox projects.
* Created to be a starting point or guide, not a complete UI framework
* Easily customizable.

## Flexible Box Layout
DescriptionCSS Flexible Box Layout, commonly known as Flexbox, is a CSS3 web layout model. It is in the W3C's candidate recommendation stage. The flex layout allows responsive elements within a container to be automatically arranged depending upon screen size.

### Preview
![SwaadBox Preview](preview.png)

## Browser Support
- This styleguide supports all browsers colored green <a href="http://caniuse.com/#search=flexbox">here</a> (on the canIuse site). For Example: this theme should <em>not</em> be used with IE 11 (and below) or Safari 8 (and below). If you need something with more backwards compatibility, checkout <a href="https://gitlab.com/sayyednoman/">SwaadBox Theme</a>.

