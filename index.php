<?php include("includes/lead.php"); ?>

    <section class="section plans">
        <span class="shadow-top"></span>
        <div class="layout">
            <div class="eyebrow">
                <h2 class="medium-title">Our Meal Plans</h2>
                <p class="lead">In this simple clean-eating meal plan for winter, we show you how to drop pounds the healthy way.</p>
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="Veg-tab" data-toggle="tab" href="#Veg" role="tab" aria-controls="Veg" aria-selected="true">Veg</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="NonVeg-tab" data-toggle="tab" href="#NonVeg" role="tab" aria-controls="NonVeg" aria-selected="false">Non-Veg</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="Veg" role="tabpanel" aria-labelledby="Veg-tab">
                    <div class="meal-wrap">
                        <label for="veg" class="meal">
                            <h3 class="medium-title">Standard <input type="radio" name="veg" id="veg"></h3>
                            <div class="copy-wrap">
                                <div class="image"><img src="https://i.pinimg.com/originals/c1/85/2c/c1852c886311bda7180b8576892db95b.jpg" alt=""></div>
                                <div class="copy">
                                    <h4 class="small-title">Included</h4>
                                    <ul class="include">
                                        <li>1 Veg Dish</li>
                                        <li>1 Daal</li>
                                        <li>4 Chapatis</li>
                                        <li>1 Rice Bowl</li>
                                        <li>Salad / Desert /Raita</li>
                                    </ul>
                                </div>
                            </div>
                        </label>
                        <label for="nonveg" class="meal">
                            <h3 class="medium-title">Mini <input type="radio" name="veg" id="nonveg"></h3>
                            <div class="copy-wrap">
                                <div class="image"><img src="https://feelgoodfoodie.net/wp-content/uploads/2019/07/One-Pan-Pasta-8.jpg" alt=""></div>
                                <div class="copy">
                                    <h4 class="small-title">Included</h4>
                                    <ul class="include">
                                        <li>1 Veg Dish</li>
                                        <li>1 Daal</li>
                                        <li>4 Chapatis</li>
                                        <li>1 Rice Bowl</li>
                                        <li>Salad / Desert /Raita</li>
                                    </ul>
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="place"><button type="submit">Place Order $46</button></div>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">dce</div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">fdfsd</div>
            </div>
        </div>
        <span class="shadow-bottom"></span>
    </section>

    <section class="section why-swaad">
        <div class="layout">
            <div class="eyebrow">
                <h3 class="medium-title">Why SwaadBox</h3>
                <p class="lead">Reveal more about our meal option</p>
            </div>
            <div class="points">
                <div class="column">
                    <span class="icon"></span>
                    <h3 class="small-title">Ghar ka khana</h3>
                    <p>Swaadisht home style food with minimal masalas & oil.</p>
                </div>
                <div class="column">
                    <span class="icon"></span>
                    <h3 class="small-title">My Swaad Box</h3>
                    <p>Manage your orders, cancellations, renewals in your account.</p>
                </div>
                <div class="column">
                    <span class="icon"></span>
                    <h3 class="small-title">Amazing Variety</h3>
                    <p>Enjoy variety of Sabzis througout the month.</p>
                </div>
                <div class="column">
                    <span class="icon"></span>
                    <h3 class="small-title">Easy Cancellation</h3>
                    <p>Cancel your meal easily * , we will credit the cancelled meal in your A/c.</p>
                </div>
                <div class="column">
                    <span class="icon"></span>
                    <h3 class="small-title">Easy Ordering</h3>
                    <p>Book your meals in less than 2 minutes on Swaad Box.</p>
                </div>
                <div class="column">
                    <span class="icon"></span>
                    <h3 class="small-title">Subscribe Options</h3>
                    <p>Try Swaad Box for 3 Days & subscribe from a week to 3 months.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="section support">
        <div class="layout">
            <div class="column">
                <div class="eyebrow">
                    <h3 class="medium-title">Support</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a class="action" href="#!">Call us @ 9819117020</a>
                </div>
                <img src="images/support.jpg" alt="">
            </div>
            <div class="column">
                <div class="eyebrow">
                    <h3 class="medium-title">Fast Delivery</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a class="action" href="#!">support@swaadbox.in</a>
                </div>
                <img src="images/delivery.jpg" alt="">
            </div>
        </div>
    </section>
    
    
    <section class="section footer">
        <div class="layout">
            <div class="copyright">Copyright © 2020 Swaadbox.in All rights reserved. Developed by : Notion Technologies</div>
            <div class="link">
                <a href="#!" class="privacy">Privacy Policy</a>
                <a href="#!" class="terms">Terms and Condition</a>
                <a href="#!" class="support">Support</a>
            </div>
            <div class="social">
                <a href="https://www.facebook.com/Swaad-Box-108834530622587/" target="_blank" class="social-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="21" height="24" viewBox="0 0 21 24">
                        <path fill="#fff" fill-rule="nonzero" d="M20.86 1.007c.172-.372-.005-.931-.536-.945C14.407-.09 6.75.38.832.147a.547.547 0 0 0-.427.159.575.575 0 0 0-.36.56C.04 8.05-.256 15.406.73 22.54c.031.228.151.362.302.421a.56.56 0 0 0 .464.214c1.134-.006 2.764-.078 3.898-.085a.565.565 0 0 0 .506-.28.579.579 0 0 0 .282-.524V15.09c0-.031-.005-.06-.008-.09.003-.026.008-.053.008-.079 0-.38-.286-.59-.622-.635-.46-.063-1.17-.079-1.633-.083-.025-.15-.053-.553-.08-.702.423-.105 1.105-.142 1.547-.146.071 0 .133-.014.191-.033.327.021.645-.177.597-.602-.23-2.046-.237-3.834-.085-5.886.834-2.049 3.03-2.777 5.062-2.517v.996C7.515 5.45 6.816 9.1 7.614 12.295c.02.08.053.146.09.204.045.273.242.516.593.516h1.535v1.018c-.508.268-1.043.193-1.617.275-.007 0-.013.004-.02.005-.323-.016-.64.184-.603.607.155 1.803.247 5.388.332 7.196.015.315.21.507.44.58a.615.615 0 0 0 .265.055c4.01.003 7.522.076 11.53-.085a.588.588 0 0 0 .493-.28.595.595 0 0 0 .294-.524c.156-6.942-.077-13.884-.083-20.826l-.002-.029zm-1.15 20.404c-3.688.132-6.88.079-10.569.071-.069-1.757-.148-4.578-.261-6.01.667-.14 1.304-.151 1.888-.512a.588.588 0 0 0 .294-.487.712.712 0 0 0 .014-.146V12.38c0-.411-.314-.615-.626-.614a.651.651 0 0 0-.162-.02H8.764c-.56-2.464-.244-5.288 2.851-5.165a.542.542 0 0 0 .572-.413.584.584 0 0 0 .216-.476V3.745c0-.414-.317-.618-.63-.614-2.884-.498-5.76.536-6.896 3.408a.56.56 0 0 0-.029.36c-.132 1.824-.137 3.386.029 5.203-.56.035-1.368.13-1.888.343-.292.12-.497.379-.447.705 0 .053.002.107.012.16.093.479.17 1.213.25 1.694.048.299.367.479.648.462.011.001.022.004.034.004.402 0 1.051.003 1.451.027v6.33c-.85.016-2.197.055-3.047.07-.893-6.731-.622-13.685-.604-20.466 5.621.185 12.8-.21 18.335-.113.011 6.698.223 13.396.089 20.093z"></path>
                    </svg>
                </a>

                <a href="https://twitter.com/@swaadBox" target="_blank" class="social-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="19" viewBox="0 0 24 19">
                        <g fill="#fff" fill-rule="nonzero">
                            <path d="M23.506 4.13c-.42-.209-.844-.412-1.258-.631.312-.498.588-1.016.835-1.551.14-.304.089-.658-.22-.84a.651.651 0 0 0-.708.056c-.702.219-1.383.494-2.054.793a4.462 4.462 0 0 0-.606-.693c-3.156-2.937-8.288-.242-9.677 2.958-2.72-.122-5.015-1.506-6.896-3.207a.622.622 0 0 0-.257-.14c-.322-.146-.753-.076-.837.33-.38 1.83.138 3.557 1.432 4.893a24.999 24.999 0 0 1-.775-.035.64.64 0 0 0-.287.043c-.42-.085-.926.195-.741.659a5.381 5.381 0 0 0 2.588 2.85c-.128.002-.256 0-.386-.004a.684.684 0 0 0-.212.025c-.386.063-.714.425-.445.809.838 1.19 1.993 2.07 3.4 2.623-.827.828-2 1.126-3.299 1.032a.765.765 0 0 0-.142.003.638.638 0 0 0-.293.073c-.274.144-.414.51-.222.758.025.032.052.061.078.092-.531-.053-1.06-.13-1.588-.238-.574-.283-1.286.42-.741.868 1.069.88 2.27 1.534 3.61 2.027.129.048.244.052.344.029 7.436 1.816 14.116-3.363 16.273-9.521.2-.57.358-1.185.457-1.814.91-.31 1.803-.663 2.695-1.02.633-.253.472-1.19-.068-1.228zm-4.694 4.829c-2.287 5.24-8.27 9.347-14.68 7.6a.697.697 0 0 0-.201-.024c-.273-.107-.54-.223-.8-.349.523.03 1.05.037 1.579.02.72-.023.787-.932.201-1.085-.015-.008-.027-.017-.043-.023 1.247-.263 2.321-.927 3.032-1.993.22-.329.015-.64-.284-.765a.667.667 0 0 0-.209-.105c-1.041-.314-1.937-.837-2.653-1.543.73-.06 1.448-.198 2.16-.38.716-.185.475-1.137-.191-1.092a.715.715 0 0 0-.221-.053C4.979 9.055 3.75 8.327 3.025 7.2c.623.025 1.246.028 1.87.028.708 0 .786-.872.236-1.068-1.33-.93-2.144-2.177-2.2-3.612 2.021 1.606 4.44 2.778 7.216 2.796a.616.616 0 0 0 .388-.12.545.545 0 0 0 .331-.343c.735-2.16 3.208-4.05 5.835-3.668 1.034.15 1.898.641 2.44 1.446 1.242 1.85.48 4.448-.33 6.3zm2.004-5.398a5.095 5.095 0 0 0-.154-.518c.192-.085.386-.167.58-.247-.103.168-.21.333-.322.495a.574.574 0 0 0-.104.27zm.167 1.482a6.76 6.76 0 0 0-.064-.926c.042.05.093.096.16.136.27.16.548.307.828.449-.306.118-.614.23-.924.341z"></path>
                            <path d="M15.267 3.049c-.824 0-.824 1.279 0 1.279.825 0 .825-1.28 0-1.28z"></path>
                        </g>
                    </svg>
                </a>

                <a href="https://www.instagram.com/swaad_box/" target="_blank" class="social-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
                        <g fill="#fff" fill-rule="nonzero">
                            <path d="M15.9 21.353H5.454A5.46 5.46 0 0 1 0 15.9V5.452A5.458 5.458 0 0 1 5.453 0H15.9a5.458 5.458 0 0 1 5.453 5.452V15.9a5.46 5.46 0 0 1-5.453 5.453zM5.454 1.22A4.236 4.236 0 0 0 1.22 5.45V15.9a4.237 4.237 0 0 0 4.233 4.232H15.9a4.236 4.236 0 0 0 4.232-4.232V5.452A4.236 4.236 0 0 0 15.9 1.22H5.454z"></path>
                            <path d="M20.743 8.012h-7.521a.61.61 0 0 1 0-1.22h7.52a.61.61 0 0 1 0 1.22zM7.973 8.012H.61a.61.61 0 0 1 0-1.22h7.363a.61.61 0 1 1 0 1.22z"></path>
                            <path d="M10.676 15.434a4.764 4.764 0 0 1-4.759-4.758 4.764 4.764 0 0 1 4.76-4.759 4.764 4.764 0 0 1 4.757 4.76 4.763 4.763 0 0 1-4.758 4.757zm0-8.296a3.544 3.544 0 0 0-3.54 3.539 3.543 3.543 0 0 0 3.54 3.538 3.541 3.541 0 0 0 3.538-3.538 3.543 3.543 0 0 0-3.538-3.54zM17.57 6.436h-2.745a.61.61 0 0 1-.61-.61V3.08a.61.61 0 0 1 .61-.61h2.745a.61.61 0 0 1 .61.61v2.746a.61.61 0 0 1-.61.61zm-2.136-1.22h1.526V3.69h-1.526v1.525z"></path>
                        </g>
                    </svg>
                </a>

            </div>
        </div>
    </section>

    <?php include("includes/footer.php"); ?>