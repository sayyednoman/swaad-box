<?php
$page_name = basename($_SERVER['PHP_SELF']);
$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $page_name);

if ($page_name=="index.php"){
    $page_title = "SEAHAWK LOGISTICS PVT LTD ";
    $page_meta = "";
    $index_active = " active";
}
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="bootstrap.css">
    <link rel="stylesheet" href="swaad.css">

    <title>Hello, world!</title>
  </head>
	<body class="page-index">
    <div class="navigation-wrapper">
        <div class="navigation-copy layout">
            <a href="#!" class="swaad-logo"></a>
            <ul class="menu">
                <li><a href="#!">About Us</a></li>
                <li><a href="#!">Plans</a></li>
                <li><a href="#!">Why Swaad Box</a></li>
                <li><a href="#!">Review</a></li>
            </ul>
            <ul class="menu right">
                <li class="online-order"><a href="#!">Online Order</a></li>
                <li class="login"><a href="#!">Login / Register</a></li>
            </ul>
        </div>
        <?php if($page_name=="index.php"){ ?>
        <div class="home-promotion">
            <div class="copy">
                <h2 class="large-title">Swaad Box</h2>
            </div>
            <figure class="image"></figure>
        </div>
        <?php } ?>
    </div>