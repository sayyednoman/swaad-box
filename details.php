<?php include("includes/lead.php"); ?>

    <div class="meal-plan-wrap">
        <div class="layout">
            <div class="meal-left">
            <div class="dvider">
                <span class="label">Select Time</span>
                <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                    <label class="btn checkbox day active">
                        <span></span>
                        <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> Lunch
                    </label>
                    <label class="btn checkbox night">
                        <span></span>
                        <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Dinner
                    </label>
                    <label class="btn checkbox day-night">
                        <span></span>
                        <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Both
                    </label>
                </div>
            </div>

            <div class="dvider-wrap">
                <div class="dvider-copy">
                    <div class="dvider">
                        <span class="label">Select Meal Type</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> Veg
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Alternate
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Select Meal Plan</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> Mini
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Standard
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Meal Duration</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> 3 Days
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> 1 Week
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> 1 Month
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> 3 Months
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Meals per week</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> Monday to Friday
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Monday to Saturday
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Quantity</span>
                        <div class="quantity">
                            <button type="submit" class="minus"></button>
                            <input class="form-control" min="1" max="10" type="text" value="1" name="" id="">
                            <button type="submit" class="plus"></button>
                        </div>
                    </div>

                </div>
                <div class="dvider-copy">
                <div class="dvider">
                        <span class="label">Select Meal Type</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> Veg
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Alternate
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Select Meal Plan</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> Mini
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Standard
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Meal Duration</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> 3 Days
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> 1 Week
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> 1 Month
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> 3 Months
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Meals per week</span>
                        <div class="btn-group btn-group-toggle form-group" data-toggle="buttons">
                            <label class="btn checkbox active">
                                <input type="radio" name="rdoType" id="rdoType1" value="I" autocomplete="off" checked=""> Monday to Friday
                            </label>
                            <label class="btn checkbox">
                                <input type="radio" name="rdoType" id="rdoType2" value="D" autocomplete="off"> Monday to Saturday
                            </label>
                        </div>
                    </div>
                    <div class="dvider">
                        <span class="label">Quantity</span>
                        <div class="quantity">
                            <button type="submit" class="minus"></button>
                            <input class="form-control" min="1" max="10" type="text" value="1" name="" id="">
                            <button type="submit" class="plus"></button>
                        </div>
                    </div>

                </div>
                <div class="charges"><b>Delivery Charges:</b> 30 per meal (For Mumbai Only).</div>
            </div>

        </div>
            <div class="meal-right">
                <div class="summary">
                    <h3 class="small-title">Menu</h3>
                    <div class="row-wrap">
                        <span class="attribute">Meal Type</span>
                        <span class="value">Veg</span>
                    </div>
                    <span class="meal-title"></span>
                    <div class="row-wrap">
                        <span class="attribute">Meal Plan</span>
                        <span class="value">Standard</span>
                    </div>
                    <div class="row-wrap">
                        <span class="attribute">Meal Duration</span>
                        <span class="value">4 Days</span>
                    </div>
                    <div class="row-wrap total">
                        <span class="attribute">Total</span>
                        <span class="value">&#x20b9;500</span>
                    </div>
                </div>
            </div>
        </div>
    </div>    

<?php include("includes/footer.php"); ?>